package me.xujichang.lib.fragments.base;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

public class LazyFragmentHelper {

    public void addFragment(FragmentManager pManager, Fragment pFragment) {

    }

    public void showFragment(FragmentManager pManager, Fragment pFragment) {

    }

    public void hideFragment(FragmentManager pManager, Fragment pFragment) {

    }

    public void loadFragments(FragmentManager pManager, Fragment pShowFragment, Fragment... pFragments) {

    }

}
